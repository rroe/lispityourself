# LIYS

LispItYourSelf is an Android application developed using idiomatic Clojure, with the goal of allowing evaluation of user input to modify the application at runtime.

## Usage

### Prerequisite Requirements
* Java Development Kit (http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
* Android SDK (https://developer.android.com/sdk/index.html#Other)
* Leiningen (http://leiningen.org/)
* Android Target API 19 (Installed Via Android SDK Manager)
* Android Support Repository (Installed Via Android SDK Manager)

### Building the project
To build the project, run:
```
lein droid build
```

To build the project and then deploy it to a connected Android device, run:
```
lein droid doall
```

__Please note that :sdk-path in project.clj must be pointing towards your Android SDK path, and that it cannot contain whitespace characters!__

### Code Documentation
To help keep things in constant form, I propose the following rules for documentation:
```clojure
(defn some-function-name
  {
    :doc "This is your doc string, explaining the function"
    :author "Author Name"
    :args {
      :arg1 "The first arg and what it is for"
      :etc "And so on and so forth"
    }
  }
  [arg1 etc]
  (some-other-function arg1 etc))
```

## License

Distributed under the Eclipse Public License, the same as Clojure.

## TODO
- [x] Implement an output system
- [x] Fix issue where ```(eval ;;etc)``` only evaluates in *clojure.core*
- [ ] Finish implementing new-linear-layout function
- [ ] Implement more wrapper functions to abstract the android out of things (and possibly move to separate, more logical namespace than main)
- [ ] Add 'repl' (previous command) history
- [ ] Add management system for saving code and executing at startup
- [ ] Create custom EditText that allows for parens-matching (maybe)
- [ ] Implement Multi-command system. EG:
```clojure
(def x 5)
(def y 7)

;; The above (currently) will only evaluate the first line, and thus will throw a RuntimeException if a user
;; attempts to access symbol y.
```
- [ ] Prettify UI, add new Logo and other resources, and other misc GUI enhancements
