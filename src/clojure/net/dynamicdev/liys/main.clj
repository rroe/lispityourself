(ns net.dynamicdev.liys.main
    (:require [neko.activity :refer [defactivity set-content-view!]]
              [neko.debug :refer [*a]]
              [neko.notify :refer [toast]]
              [neko.resource :as res]
              [neko.find-view :refer [find-view]]
              [neko.threading :refer [on-ui]])
    (:import android.widget.EditText))

(res/import-all)

(defn print-out
  {
    :doc "Takes in an input and prints it to the repl-output box."
    :author "Richard Roe"
    :args {
      :message "The message to be put in the output field"
      :activity "The current activity"
    }
  }
  [message activity]
  (.setText (find-view activity ::repl-out) message))

(defn handle-user-input
  {
    :doc "Handles when a user inputs code into the editor, and attempts to run the code.
    Displays a toast if an Exception is raised."
    :author "Richard Roe"
    :args {
      :code "The entered code from the EditText element"
      :activity "The current Activity"
    }
  }
  [code activity]
  (try
    (let [cur-ns (find-ns 'net.dynamicdev.liys.main)]
      (def result (binding [*ns* cur-ns] (eval (read-string (.toString code)))))
      (if (not (empty? (str result)))
          (print-out (str result) activity)))
     (catch Exception exception
       (print-out
           (str "Exception: " (.getMessage exception)) activity))))

(defn notify-from-edit
  {
    :doc "Finds an EditText element with ID ::user-input in the given activity. Gets
    its contents and handles checking if empty; displays toast if so, otherwises
    passes off input for evaluation."
    :author "Richard Roe"
    :args {
      :activity "The current activity"
    }
  }
  [activity]
  (let [^EditText input (.getText (find-view activity ::user-input))]
    (if (empty? input)
        (print-out
             (res/get-string R$string/input_is_empty) activity)
        (handle-user-input input activity))))

(defactivity net.dynamicdev.liys.BuildActivity
  :key :main
  (onCreate [this bundle]
    (.superOnCreate this bundle)
    (neko.debug/keep-screen-on this)
    (def activity (*a))
    (on-ui
      (set-content-view! (*a)
        [:linear-layout {:orientation :vertical
                         :id ::build-layout
                         :layout-width :fill
                         :layout-height :wrap}
         [:edit-text {:id ::user-input
                      :hint R$string/help_hint
                      :layout-width :fill}]
          [:button {:text R$string/touch_me
                      :layout-width :fill
                      :id ::eval-button
                      :on-click (fn [_] (notify-from-edit (*a)))}]
          [:edit-text {:id ::repl-out
                       :layout-width :fill}]]))
      (.setEnabled (find-view (*a) ::repl-out) false)))

(defn list-ids
  {
    :doc "Displays all initial id's in use."
    :author "Richard Roe"
  }
  []
  ;; Yes, I'm aware this is gross.
  (print-out "::build-layout - Activity layout\n::user-input - Input box\n::eval-button - Evaluate! button\n::repl-out - Output box" activity))

(defn help
  {
    :doc "Displays help message."
    :author "Richard Roe"
  }
  []
  (let [help-messages [
    "Any valid Clojure entered will be evaluated.\n"
    "All code entered is evaluated in the net.dynamicdev.liys.main namespace.\n"
    "To reference the current activity (BuildActivity), use the Var 'activity'.\n"
    "For a list of id's in initial use, use '(list-ids)'"
    ]]
  (print-out (clojure.string/join help-messages) activity)))

;; I should probably think about moving some of these util functions to another namespace

(defn send-text
  {
    :doc "Utility function to send a text message to the given number"
    :author "Richard Roe"
    :args {
     :number "The phone number of the recipient"
     :message "The message to send"
     }
   }
  [number message]
  (.sendTextMessage (android.telephony.SmsManager/getDefault) number nil message nil nil))

(defn new-linear-layout
  {
    :doc "Updates the activity's view with a new linear layout, based on the supplied args"
    :author "Richard Roe"
    :args {
      :id "The id of the new layout"
      :elements "A vector of view elements to be applied to the layout"
    }
  }
  [id elements]
  (print-out "Not yet implemented!" activity))
